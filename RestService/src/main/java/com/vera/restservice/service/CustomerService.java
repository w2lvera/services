/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restservice.service;

import com.vera.restservice.CustomerDAO;
import com.vera.restservice.model.Customer;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Wera
 */
@Path("/customer")
public class CustomerService {

    private CustomerDAO customerDAO;

    @GET
    @Path("/{param}")
    public Response getMsg(@PathParam("param") String msg) {

        String output = "Jersey say : " + msg;

        return Response.status(200).entity(output).build();

    }

    /**
     http://localhost:8080/restservice/rest/customer/all
     * restservice - final name from pom.xml, build section
     * rest - uri mapping from web.xml
     * customer - from path
     * all - method
     **/
    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public java.util.List<Customer> getAllCustomers() {

        java.util.List<Customer> list = customerDAO.getAllCustomers();

        return list;

    }

    public void setCustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }

}
