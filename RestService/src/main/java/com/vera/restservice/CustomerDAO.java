/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restservice;

import com.vera.restservice.model.Customer;
import java.util.List;

/**
 *
 * @author Wera
 */
public interface CustomerDAO {
    public List<Customer> getAllCustomers();
}
