/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restservice.model;

/**
 *
 * @author Wera
 */
public class Customer {
    private String name;
    private String city;
    private String address; 

    public Customer(String name) {
        this.name = name;
        this.city = "Tver";
        this.address = "XZ";
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
}
