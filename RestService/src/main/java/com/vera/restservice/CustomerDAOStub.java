/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restservice;

import com.vera.restservice.model.Customer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Wera
 */
public class CustomerDAOStub implements CustomerDAO {

    @Override
    public List<Customer> getAllCustomers() {
        final List<Customer> list =  new ArrayList<Customer>(
                Arrays.asList(new Customer("dgsdfgKate"), new Customer("Lena")));
        return list;
    }   
    
}
