/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restservicemvc.service;

import com.vera.restservicemvc.model.Greeting;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Wera
 */
@RestController
public class GreetingService {
    
    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    /****
     * http://localhost:9090/RestServiceMVC/greeting?name=qwerty
     * @param name from URL
     * @return JSON parsed from Greeting object
     */
    @RequestMapping(path = "/greeting", method = GET)
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
}
