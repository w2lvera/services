/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restservicemvc.service;

import com.vera.restservicemvc.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Wera
 */
@RestController
@RequestMapping(path = "/customer")
public class CustomerService {
    
    
    private final CustomerDAO customerDAO;

    @Autowired
    public CustomerService(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }
    
    
    @RequestMapping(path = "/all", method = GET)
    public java.util.List<Customer> getAllCustomers() {
        return customerDAO.getAllCustomers();  
    }
    
    @RequestMapping(path = "/", method = PUT)
    public void addNewCustomer(@RequestBody com.vera.restservicemvc.view.Customer externalCustomer){
        System.out.println(externalCustomer.getName());
    }
}
