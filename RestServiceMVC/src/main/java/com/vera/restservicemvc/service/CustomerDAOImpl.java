/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restservicemvc.service;

import com.vera.restservicemvc.model.Customer;
import com.vera.restservicemvc.model.CustomerRowMapper;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Wera
 */

@Repository
public class CustomerDAOImpl implements CustomerDAO {

    private final JdbcTemplate jdbcTemplateObject;

    @Autowired
    public CustomerDAOImpl(DataSource dataSource) {        
        this.jdbcTemplateObject = new JdbcTemplate(dataSource);
        this.jdbcTemplateObject.setResultsMapCaseInsensitive(true);
    }
 
    @Override
    public List<Customer> getAllCustomers() {
        String SQL = "select * from customer";
        List<Customer> customers = jdbcTemplateObject.query(SQL, new CustomerRowMapper());
        return customers;
    }

}
