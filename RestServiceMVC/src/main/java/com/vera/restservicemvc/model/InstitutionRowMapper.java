package com.vera.restservicemvc.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author wera
 */
public class InstitutionRowMapper implements RowMapper<Institution>{
    
    public Institution mapRow(ResultSet rs, int rowNum) throws SQLException {
        Institution c = new Institution(rs.getString("name"),
                rs.getString("address"),
                rs.getLong("latitude"),
                rs.getLong("longitude"));
        c.setId(rs.getLong("id"));
        return c;
    }

}
