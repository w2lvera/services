package com.vera.restservicemvc.model;

/**
 *
 * @author wera
 */
public class Institution {
    Long id;
    String name;
    String address;
    long latitude;
    long longitude;

    public Institution(String name, String address, long latitude, long longitude) {
        this.name = name;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public long getLatitude() {
        return latitude;
    }

    public long getLongitude() {
        return longitude;
    }  

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }    
    
}
