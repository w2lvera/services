/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.restservicemvc.model;

/**
 *
 * @author Wera
 */
public class Customer {
    private final String name;
    private final String city;
    private final String address; 

    public Customer(String name) {
        this.name = name;
        this.city = "Tver";
        this.address = "XZ";
    }

    public Customer(String name, String city, String address) {
        this.name = name;
        this.city = city;
        this.address = address;
    }
    
    public String getName() {
        return name;
    }   

    public String getCity() {
        return city;
    }    

    public String getAddress() {
        return address;
    }

    /* immutable object, no setters
    public void setCity(String city) {
        this.city = city;
    }    
    public void setAddress(String address) {
        this.address = address;
    }    
     public void setName(String name) {
        this.name = name;
    }*/
}
